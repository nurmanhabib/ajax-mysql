-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 5.5.27 - MySQL Community Server (GPL)
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table skriptif.mahasiswa
CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nim` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `no_hp` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mahasiswa_nim_unique` (`nim`),
  UNIQUE KEY `mahasiswa_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table skriptif.mahasiswa: ~4 rows (approximately)
DELETE FROM `mahasiswa`;
/*!40000 ALTER TABLE `mahasiswa` DISABLE KEYS */;
INSERT INTO `mahasiswa` (`id`, `nim`, `nama`, `email`, `no_hp`, `user_id`, `created_at`, `updated_at`) VALUES
    (13, '08018298', 'Galih Pamungkas', 'galih@skripsi.tif.uad.ac.id', '08123456789', 7, '2015-04-03 19:27:16', '2015-05-06 18:03:56'),
    (14, '08018093', 'Fiki Justisia Bahayangkara', 'fiki@skripsi.tif.uad.ac.id', '08123456789', 8, '2015-04-03 19:27:17', '2015-05-06 18:03:56'),
    (15, '07018096', 'Dicky Martin Pramanta', 'dicky@skripsi.tif.uad.ac.id', '08123456789', 9, '2015-04-03 19:27:17', '2015-05-06 18:03:57'),
    (16, '11018099', 'Habib Nurrahman', 'nurmanhabib@gmail.com', '085228830038', 10, '2015-04-03 23:53:25', '2015-05-06 18:03:57'),
    (17, '11018055', 'Budi Sumarman', 'nurmanhabib4@gmail.com', '085228830038', 13, '2015-05-06 18:26:34', '2015-05-06 18:26:34');
/*!40000 ALTER TABLE `mahasiswa` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
