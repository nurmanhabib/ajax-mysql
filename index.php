<html>
<head>
    <title>Load From MySQL</title>
</head>

<body>

    <form action="">
        NIM = <input type="text" name="nim" id="nim"> <br><br>
        Nama = <input type="text" name="nama" id="nama"> <br><br>
        Email = <input type="text" name="email" id="email"> <br><br>
    </form>
    
    <script src="js/jquery-2.0.2.min.js"></script>
    <script>
    $(document).ready(function() {

        $('#nim').keyup(function() {

            var nim = $(this).val()

            $.get('getData.php', {nim: nim})
                .success(function(data) {
                    var json = $.parseJSON(data)
                    
                    $('#nama').val(json.nama)
                    $('#email').val(json.email)
                })

        })
    })
    </script>
</body>
</html>